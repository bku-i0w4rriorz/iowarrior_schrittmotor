﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.Windows;
using System.Collections.Generic;

namespace Terminplaner
{
    public class logentry
    {
        public DateTime timestamp;
        public string message;
        public int ticks;
    }

    static class editlogentry
    {
        #region VARS
        static string filepath = @".\logentrys";
        static List<logentry> data = new List<logentry>();
        #endregion
        #region functions
        #region load
        private static List<logentry> load()
        {
            List<logentry> EntryList = new List<logentry>();

            XmlSerializer serializer = new XmlSerializer(typeof(List<logentry>));
            if (File.Exists(filepath))
            {
                TextReader reader = new StreamReader(filepath);
                List<logentry> helpTemp = (List<logentry>)serializer.Deserialize(reader);
                reader.Close();


                foreach (var entry in helpTemp)
                {
                    logentry item = new logentry()
                    {
                        timestamp = entry.timestamp,
                        message = entry.message,
                        ticks = entry.ticks,
                    };
                    EntryList.Add(item);
                }

                return EntryList;
            }
            else
            {
                MessageBox.Show("FEHLER");
                return null;
            }
        }
        #endregion
        #region write
        public static void write(List<logentry> logentry)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<logentry>));
            TextWriter writer = new StreamWriter(filepath);
            serializer.Serialize(writer, logentry);
            writer.Close();
        }
        #endregion
        #region delete
        public static void delete(string test)
        {
            string filepath = @".\logentrys";

            if (File.Exists(filepath))
            {
                File.Delete(filepath);
            }
            else
            {
                MessageBox.Show($@"Die Datei '{filepath}' konnte nicht durch das Programm gelöscht werden, es könnte sein dass das Programm nicht über genügend Rechte verfügt, oder das ein fehlhafter Pfad angegeben wurde, bitte überprüfen Sie den Inhalt des Programmordners '\logentrys'");
            }
        }
        #endregion
        #endregion
    }
}