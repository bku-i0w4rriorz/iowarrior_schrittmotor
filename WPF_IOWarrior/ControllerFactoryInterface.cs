﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_IOWarrior
{
    public interface ControllerFactoryInterface
    {
        ControllerInterface create();
    }
    public class IOWarriorControllerFactory : ControllerFactoryInterface
    {
        public ControllerInterface create()
        {
            return new IOWarriorController();
        }
    }
    /* implemented for testing purposes */
    //class TestControllerFactory : ControllerFactoryInterface
    //{
    //    ControllerInterface ControllerFactoryInterface.create()
    //    {
    //        return new TestController();
    //    }
    //}
}
