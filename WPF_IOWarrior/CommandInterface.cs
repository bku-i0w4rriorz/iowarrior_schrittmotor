﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_IOWarrior
{
    public enum CommandType
    {
        Rotate = 0,
        SetSpeed,
        StopStart
    }
    public enum Speed
    {
        Slow = 0,
        Fast
    }
    /*!
     * \brief Die Struktur für einen Befehl.
     *
     * **Autor: Georg Hirsch**
     */
    public struct CommandData
    {
        public CommandType m_type;
        public Rotation m_rotation;
        public Speed m_speed;
        public bool m_running;
    }
}
