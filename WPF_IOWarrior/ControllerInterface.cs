﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_IOWarrior
{
    /*!
      * \brief Das Interface für einen Controller
      *
      * **Autor des Interfaces: Georg Hirsch**
      */
    public interface ControllerInterface
    {
        void init();
        bool setPinConfiguration(byte row1, byte row2 = 0);
        bool sendCommand(CommandData command);
        void processResponse(byte[] response);
    }
}
