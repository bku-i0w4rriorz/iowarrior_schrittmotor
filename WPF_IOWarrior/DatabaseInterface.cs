﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_IOWarrior
{
    public interface DatabaseInterface
    {
        void setDatabase(string database);
        void log(string message);
        //void get(string group, string key);
        void get();
    }
}
