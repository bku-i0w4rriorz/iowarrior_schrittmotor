﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_IOWarrior
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        private ControllerFactoryInterface m_controllerFactory;
        private ControllerInterface m_controller;
        private DatabaseInterface m_database;
        private CommandData m_commandData;
		public MainWindow()
		{
            m_controllerFactory = new IOWarriorControllerFactory();
            m_controller = m_controllerFactory.create();
            m_controller.init();
            m_controller.setPinConfiguration(3, 240);
			//CommandData data = new CommandData();
			//data.m_type = CommandType.Rotate;
   //         data.m_rotation = new Rotation((int)RadialSlider);
   //         RotateCommand command = new RotateCommand();
			//command.setData(data);
			//m_controller.sendCommand(command);

   //         data.m_type = CommandType.SetSpeed;
   //         data.m_speed = Speed.Fast;
   //         SetSpeedCommand speedCommand = new SetSpeedCommand();
   //         speedCommand.setData(data);
   //         m_controller.sendCommand(speedCommand);
            /* TODO: initialize controller */
            
            //m_database = new SimpleFileDatabase();
            //m_database.setDatabase("..\\..\\..\\xml_sample.xml");
            m_database = new MySqlDatabase();
            m_database.setDatabase("MySqlDatabase.cs");
            m_database.get();
			InitializeComponent();

			
			//start();
		}

        public void configureRotation(Rotation rotation)
        {

        }

        public void newCommand(CommandData data)
        {

        }

        private void ToggleButton_fast(object sender, RoutedEventArgs e)
        {
            m_commandData.m_type = CommandType.SetSpeed;
            m_commandData.m_speed = Speed.Fast;
            m_controller.sendCommand(m_commandData);
        }

        private void ToggleButton_slow(object sender, RoutedEventArgs e)
        {
            m_commandData.m_type = CommandType.SetSpeed;
            m_commandData.m_speed = Speed.Slow;
            m_controller.sendCommand(m_commandData);
        }

        private void Button_run(object sender, RoutedEventArgs e)
        {
            //RadialSlider
            m_commandData.m_type = CommandType.StopStart;
            m_commandData.m_running = !m_commandData.m_running;
            m_commandData.m_rotation = new Rotation((int)RadialSlider);
            m_controller.sendCommand(m_commandData);
        }

        #region RadialSlider
        public static double RadialSlider;

		private bool _isPressed = false;
		private Canvas _templateCanvas = null;

		private void Ellipse_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			//Enable moving mouse to change the value.
			_isPressed = true;
		}

		private void Ellipse_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			//Disable moving mouse to change the value.
			_isPressed = false;
		}

		private void Ellipse_MouseMove(object sender, MouseEventArgs e)
		{
			if (_isPressed)
			{
				//Find the parent canvas.
				if (_templateCanvas == null)
				{
					_templateCanvas = MyHelper.FindParent<Canvas>(e.Source as Ellipse);
					if (_templateCanvas == null) return;
				}
				//Canculate the current rotation angle and set the value.
				const double RADIUS = 150;
				Point newPos = e.GetPosition(_templateCanvas);
				double angle = MyHelper.GetAngleR(newPos, RADIUS);
				knopf.Value = (knopf.Maximum - knopf.Minimum) * angle / (2 * Math.PI);
			}
		}
    }

    //The converter used to convert the value to the rotation angle.
    public class ValueAngleConverter : IMultiValueConverter
	{
		#region IMultiValueConverter Members

		public object Convert(object[] values, Type targetType, object parameter,
					  System.Globalization.CultureInfo culture)
		{
			double value = (double)values[0];
			double minimum = (double)values[1];
			double maximum = (double)values[2];

			MainWindow.RadialSlider = value*36;
			return MyHelper.GetAngle(value, maximum, minimum);
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter,
			  System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	//Convert the value to text.
	public class ValueTextConverter : IValueConverter
	{
		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter,
				  System.Globalization.CultureInfo culture)
		{
			double v = (double)value;
			return String.Format("{0:F2}", v);
		}

		public object ConvertBack(object value, Type targetType, object parameter,
			System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	public static class MyHelper
	{
		//Get the parent of an item.
		public static T FindParent<T>(FrameworkElement current)
		  where T : FrameworkElement
		{
			do
			{
				current = VisualTreeHelper.GetParent(current) as FrameworkElement;
				if (current is T)
				{
					return (T)current;
				}
			}
			while (current != null);
			return null;
		}

		//Get the rotation angle from the value
		public static double GetAngle(double value, double maximum, double minimum)
		{
			double current = (value / (maximum - minimum)) * 360;
			if (current == 360)
				current = 359.999;

			return current;
		}

		//Get the rotation angle from the position of the mouse
		public static double GetAngleR(Point pos, double radius)
		{
			//Calculate out the distance(r) between the center and the position
			Point center = new Point(radius, radius);
			double xDiff = center.X - pos.X;
			double yDiff = center.Y - pos.Y;
			double r = Math.Sqrt(xDiff * xDiff + yDiff * yDiff);

			//Calculate the angle
			double angle = Math.Acos((center.Y - pos.Y) / r);
			Console.WriteLine($"r:{r},y:{pos.Y},angle:{angle}., Value:{MainWindow.RadialSlider}");
			if (pos.X < radius)
				angle = 2 * Math.PI - angle;
			if (Double.IsNaN(angle))
				return 0.0;
			else
				return angle;
		}
		#endregion
	}
}