﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPF_IOWarrior
{
    public class Rotation
    {
        private int m_degrees;

        public int Degrees
        {
            get { return m_degrees; }
            set {
                while (value < -360)
                    value += 360;
                while (value >= 360)
                    value -= 360;
                m_degrees = value;
            }
        }
        
        public Rotation(int degrees)
        {
            m_degrees = degrees;
        }


    }
}
