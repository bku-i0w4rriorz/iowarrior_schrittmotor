﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using static IOWarrior.Functions;
using IOWarrior;
using System.Threading;

namespace WPF_IOWarrior
{
    /*!
     * \brief Klasse, die den Controller für einen IOWarrior darstellt.
     *
     * **Autor dieser Klasse: Georg Hirsch**
     * Die Klasse leitet vom ControllerInterface ab um das Programm modular gestalten zu können
     */
    class IOWarriorController : ControllerInterface
    {
        /*! Es wird eine Liste von Threads gebraucht, die ständig neue Threads erhält und welche löscht. So wird sichergestellt, dass dauerhaft
         * auf den IOWarrior Ports gelauscht wird */
        private List<Thread> m_listenThreads;
        /*! Das Delegat ruft die Funktion auf, welche auf den Ports des IOWarriors lauscht. So wird der Programmablauf nie geblockt */
        private ThreadStart m_listenDelegate;
        /*! Der Timer steuert mit seinem Timeout-Event wann Signale zum IOWarrior gesendet werden */
        private System.Timers.Timer m_writeTimer;
        /*! Stellt das Byte dar, welches im Interval zum IOWarrior gesendet wird */
        byte[] m_input;
        /*! Programmsprachenunabhängiger Zeiger auf den IOWarrior */
        private IntPtr m_iow;
        /*! Seriennummer des IOWarriors */
        private string m_serialNumber;
        /*! Stellt die Pins dar, welche benutzt werden um Daten vom Controller zu empfangen */
        private byte m_readPins;
        /*! Stellt die Pins dar, welche benutzt werden um Signale zum Controller zu senden */
        private byte m_writePins;

        /*!
         * \brief Konstruktor initialisiert den ganzen Controller mit Standardwerten
         *
         * Attribut             | Wie es konfiguriert wird
         * -------------------- | --------------------------
         * m_listenThreads      | Wird als leere Threadliste initialisiert (List<Thread>) [Mehr Infos zu Listen](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1.-ctor?f1url=https%3A%2F%2Fmsdn.microsoft.com%2Fquery%2Fdev15.query%3FappId%3DDev15IDEF1%26l%3DEN-US%26k%3Dk(System.Collections.Generic.List%601.%2523ctor);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.6.1);k(DevLang-csharp)%26rd%3Dtrue&view=netframework-4.7.2)
         * m_listenDelegate     | Wird als Delegat für @ref listen() konfiguriert [Mehr Infos zu ThreadStart](https://docs.microsoft.com/en-us/dotnet/api/system.threading.threadstart?f1url=https%3A%2F%2Fmsdn.microsoft.com%2Fquery%2Fdev15.query%3FappId%3DDev15IDEF1%26l%3DEN-US%26k%3Dk(System.Threading.ThreadStart);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.6.1);k(DevLang-csharp)%26rd%3Dtrue&view=netframework-4.7.2)
         * m_writeTimer         | Timer mit 10ms Interval. Zum Elapsed Event wird die @ref write() Methode hinzugefügt. AutoReset = true, Enabled = false [Mehr Infos zum Timer](https://docs.microsoft.com/en-us/dotnet/api/system.timers.timer?f1url=https%3A%2F%2Fmsdn.microsoft.com%2Fquery%2Fdev15.query%3FappId%3DDev15IDEF1%26l%3DEN-US%26k%3Dk(System.Timers.Timer);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.6.1);k(DevLang-csharp)%26rd%3Dtrue&view=netframework-4.7.2)
         * m_iow                | Wird als neuer IOWarrior initialisiert. [Mehr zur IOWarrior API](https://drive.google.com/file/d/1TcL0KXponNHE2ClOj7eHDgImrEVp82YJ/view?usp=sharing). Alle Pins werden auf 0 gesezt
         * m_serialNumber       | Wird mit der Seriennummer vom IOWarrior initialisiert. [Mehr zur IOWarrior API](https://drive.google.com/file/d/1TcL0KXponNHE2ClOj7eHDgImrEVp82YJ/view?usp=sharing)
         * m_input              | byte[5]. Alle Einträge sind 0. [Mehr zu bytes](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/byte?f1url=https%3A%2F%2Fmsdn.microsoft.com%2Fquery%2Fdev15.query%3FappId%3DDev15IDEF1%26l%3DEN-US%26k%3Dk(byte_CSharpKeyword)%3Bk(TargetFrameworkMoniker-.NETFramework%2CVersion%3Dv4.6.1)%3Bk(DevLang-csharp)%26rd%3Dtrue)
         * m_readPins           | Die Lese-Pins werden standardmäßig auf 3 gesetzt. Das bedeutet, dass die ersten 2 Pins Daten vom Board empfangen. [Mehr zur IOWarrior API](https://drive.google.com/file/d/1TcL0KXponNHE2ClOj7eHDgImrEVp82YJ/view?usp=sharing)
         * m_writePins          | Die Schreib-Pins werden standardmäßig auf 60 gesezt. So werden die Pins 3 - 6 zum Schreiben benutzt. [Mehr zur IOWarrior API](https://drive.google.com/file/d/1TcL0KXponNHE2ClOj7eHDgImrEVp82YJ/view?usp=sharing)
         */
        public IOWarriorController()
        {
            m_listenThreads = new List<Thread>();
            m_listenDelegate = new ThreadStart(this.listen);
            m_writeTimer = new System.Timers.Timer(10);
            m_writeTimer.Elapsed += write;
            m_writeTimer.AutoReset = true;
            m_writeTimer.Enabled = false;
            m_iow = IowKitOpenDevice();
            StringBuilder serialBuilder = new StringBuilder();
            IowKitGetSerialNumber(m_iow, serialBuilder);
            m_serialNumber = serialBuilder.ToString();
            m_input = new byte[5] { 0, 0, 0, 0, 0 };
            IowKitWrite(m_iow, 0, new byte[] { 0, 255, 255, 0, 0 }, 5);
            m_readPins = 3;
            m_writePins = 60;
        }
        /*!
         * \brief Im Destruktor wird der IOWarrior geschlossen und das Programm sauber beendet
         */
        ~IOWarriorController()
        {
            if (m_iow != null)
                IowKitCloseDevice(m_iow);
        }
        /*!
         * \brief Initialisiert den Controller.
         *
         * Es wird die Seriennummer des IOWarriors in der Konsole ausgegeben. Außerdem wird der erste Thread gestartet,
         * welcher auf den IOWarrior Ports lauscht. Der Thread kontrolliert von da an selbst, dass weitere Threads
         * gestartet und unnötige Threads gelöscht werden.
         */
        public void init()
        {
            Console.WriteLine("Seriennummer: {0}", m_serialNumber);
            m_listenThreads.Add(new Thread(m_listenDelegate));
            m_listenThreads.Last().Start();
        }
        /*!
         * \brief setzt die Lese- und Schreib-Pins des IOWarriors über eine Maske
         *
         * Die einzelnen Bits in der Maske repräsentieren, welche Pins auf dem IOWarrior gemeint sind. Es wird nur die erste lane
         * des IOWarriors beachtet. Die Zuweisung ist dabei 1 = Port 0.0, 2 = Port 0.1, 3 = Ports 0.0 & 0.1. Das ganze zieht sich durch das ganze Bit
         *
         * **Bei der Konfiguration muss man darauf achten, dass die beiden Masken keine Bits, die sich überschneiden**
         * \param read byte übernimmt ein Byte als Maske für die Lese-Pins. Mindestens 2 Bit müssen gesetzt sein. Maximal 4. Es werden nur die ersten 2 Bit verwertet
         * \param write byte übernimmt ein Byte als Makse für die Schreib-Pins. Mindestens 4 Bits müssen gesetzt sein. Maximal 6. Es werden nur die ersten 4 Bits verwertet
         * \return Liefert "false", falls die Masken nicht gesetzt werden können. Das ist der Fall wenn die größe einer Maske nicht gültig ist oder die beiden Masken Bits
         * beinhalten, welche sich überschneiden
         */
        public bool setPinConfiguration(byte read, byte write)
        {
            /* return false if read & write have overlapping bits */
            if ((read & write) != 0)
                return false;
            int readCount = 0;
            for (int i = 0; i < 8; ++i)
            {
                if ((read & (1 << i)) != 0)
                    ++readCount;
            }
            /* return false if read-bits arent enough or dont leave enough place for write bits */
            if ((readCount < 2) || (readCount > 4))
                return false;

            int writeCount = 0;
            for (int j = 0; j < 8; ++j)
            {
                if ((write & (1 << j)) != 0)
                    ++writeCount;
            }
            /* return if write-bits arent enough or dont leave enough place for read bits */
            if ((writeCount < 4) || (writeCount > 6))
                return false;
            /* set the bits */
            m_readPins = read;
            m_writePins = write;
            m_input[1] = 0;
            /* TODO: set the continuous (3rd) and interval (4th) write pins to true */
            setPin(3, true);
            setPin(4, true);
            Console.WriteLine("read pins are: " + m_readPins);
            Console.WriteLine("write pins are: " + m_writePins);
            return true;
        }
        /*!
         * \brief Stellt den m_input Wert für das Signal an den IOWarrior ein. m_timer gibt vor wann gesendet wird.
         *
         * Die Funktion macht starken Gebrauch von setPin(). Die Pins haben in ihrer Reihenfolge alle eine Funktion:
         *
         * * Der erste Pin steuert in welche Richtung der Motor sich dreht
         * * Der zweite Pin steuert wie schnell der Motor sich dreht
         * * Der dritte Pin versorgt den Motor mit Strom
         * * Der vierte Pin sendet Signale an die Magnete vom Motor. Er wird in einem Interval angesprochen
         *
         * Die Pins werden durch m_writePins vorgegeben. Die Funktion fragt nur aus den überreichten Daten ab, was
         * wohin gesendet werden soll
         * \param command CommandData Die Daten für den Befehl. Hier steht drin, was für ein Befehl gewünscht ist und welche Werte er hat.
         * \return liefert "false", falls ein Befehl ungültig war oder eine Einstellung nicht auf einen Pin angewendet werden konnte
         */
        public bool sendCommand(CommandData command)
        {
            switch (command.m_type)
            {
                case CommandType.Rotate:
                    /* TODO: order iowarrior to rotate */
                    /* write 1 to first pin for left. 0 for right */
                    Console.WriteLine("sending {0}° Rotation command", command.m_rotation.Degrees);
                    if (command.m_rotation.Degrees == 0)
                    {
                        return true;
                    }
                    else if (command.m_rotation.Degrees > 0)
                    {
                        return setPin(1, false);
                    }
                    else
                    {
                        return setPin(1, true);
                    }
                case CommandType.SetSpeed:
                    switch (command.m_speed)
                    {
                        case Speed.Fast:
                            /* set the second input pin to true */
                            if (!setPin(2, true))
                            {
                                Console.WriteLine("[ERROR] could not set pin No {0} to {1}. Couldn't set speed to fast", 2, true);
                                return false;
                            }

                            Console.WriteLine("switching speed to fast");
                            return true;
                        case Speed.Slow:
                            /* set the second input pin to false */
                            if (!setPin(2, false))
                            {
                                Console.WriteLine("[ERROR] could not set pin No {0} to {1}. Couldn't set speed to slow", 2, false);
                                return false;
                            }
                            Console.WriteLine("switching speed to slow");
                            return true;
                        default:
                            Console.WriteLine("[ERROR] unknowo rotation type received");
                            return false;
                    }
                case CommandType.StopStart:
                    if (command.m_running)
                    {
                        m_writeTimer.Enabled = true;
                    }
                    else
                    {
                        m_writeTimer.Enabled = false;
                    }
                    return true;
                default:
                    Console.WriteLine("[ERROR] tried so send unknown Command");
                    return false;
            }
        }
        /*!
         * \brief [UNFERTIG] verarbeitet einen Response vom IOWarrior
         *
         * **Die Funktion ist noch nicht fertig implementiert**
         *
         * Die Funktion filtert erst durch eine Bit-Operation die Pins, welche zum Schreiben genutzt werden.
         * So wird verhindert, dass das Programm auf einen Input reagiert den es eigentlich selbst generiert hat.
         * Filter-Statement:
         *
         * `response[1] = (byte)(~m_writePins & (byte)~response[1]);`
         *
         * das Statement schreibt den Wert in response[1] um, so, dass nur noch die bits auf 1 sind, die einen Pin repräsentieren, der gelesen wird
         * und auch ein Signal empfängt.
         * Der response muss "getiltet" werden, weil der IOWarrior diesen umgekehrt sendet. Heißt 254 bedeutet Pin 1 ist an. Nach Logik heißt 1 aber, dass Pin 1 an ist.
         * Hier wird logisch und verknüpft mit dem "getiltetem" Wert der m_writePins. So werden die Write-Pins schonmal ausgeschlossen. Übrig bleiben nur noch die Lese-Pins
         *
         * *Als nächstes müsste die Funktion Lese-Pin 1 und 2 aus response[1] trennen, allerdings wurde das noch nicht implementiert*
         *
         * \param response byte[] Das Array von Bytes, welches vom IOWarrior gelesen wurde. Wichtig ist nur response[1], weil wird lediglich die erste lane betrachten
         */
        public void processResponse(byte[] response)
        {   /* check if read input is from writing pin */
            response[1] = (byte)(~m_writePins & (byte)~response[1]);
            if (response[1] == 0)
                return;

            /* TODO: filter first read pin and second read pin */
            //int byteCount = 0;
            //for (int i = 0; i < 8; ++i)
            //{
            //    if ((m_readPins & (1 << i)) != 0)
            //    {
            //        if ((m_readPins ^ response[1]) != 0)
            //        {
            //            Console.WriteLine("Read Pin No " + ++byteCount + " is " + ((m_readPins & response[1]) != 0));
            //        }
            //    }
            //}
            Console.WriteLine("response read: [{0}, {1}, {2}, {3}, {4}]", response[0], response[1], response[2], response[3], response[4]);
        }
        /*!
         * \brief Funktion, welche schaut ob Änderungen auf einem IOWarrior Port passiert sind.
         *
         * Diese Funktion läuft in einem Thread. Am Ende legt sie einen neuen Thread an, der sie wieder aufruft. Nach 1 Sekunde wird der nächste Thread
         * gestartet und die Funktion löscht den Thread in dem sie selber läuft. So gibt es immer maximal 2 Threads und es ist immer einer aktiv.
         */
        private void listen()
        {
            byte[] buffer = new byte[5] { 0, 0, 0, 0, 0 };
            IowKitRead(m_iow, 0, buffer, 5);
            processResponse(buffer);

            /* start new thread. delete old one */
            m_listenThreads.Add(new Thread(m_listenDelegate));
            Thread.Sleep(1000);
            m_listenThreads.Last().Start();
            m_listenThreads.RemoveAt(0);
        }
        /*!
         * \brief Funktion sendet ein Signal an den IOWarrior
         *
         * Diese Funktion wurde angepasst damit Sie in das Delegat vom Timeout-Event des m_writeTimer gepackt werden kann.
         * So werden Daten an den IOWarrior im Interval gesendet. Die restliche Programmlogik ändert nur die Daten, die gesendet werden.
         */
        private void write(Object source, ElapsedEventArgs e)
        {
            /* TODO: negate the 4th write pin, because it's triggered in an interval */
            int bitCount = 0;
            for (int i = 0; i < 8; ++i)
            {
                /* the 4th occurence of this is needed */
                if ((m_writePins & (1 << i)) != 0)
                {
                    /* checks if we iterated often enough */
                    if (++bitCount == 4)
                    {
                        m_input[1] = (byte)(m_input[1] ^ (byte)(1 << i));
                    }
                }
            }
            IowKitWrite(m_iow, 0, new byte[]{ m_input[0], (byte)~m_input[1], (byte)~m_input[2], m_input[3], m_input[4] }, 5);
        }
        /*!
         * \brief Schreibt einen Zustand auf einen der Schreib-Pins auf dem IOWarrior
         *
         * Diese Funktion bedient sich sehr stark an binären Operatioren. Sie läuft in einer Schleife 1x für jedes Bit in m_writePins durch.
         * Dabei läuft die Schleife einfach nur 8x durch 'for (int i = 0; i < 8; ++i)`
         * Getestet, ob ein Pin auf 1 ist wird mit einer logischen UND-Verknüpung: `if ((m_writePin & (1 << i)) != 0)`
         * Das Statement bewegt die 1 im Byte bei jedem Schleifendurchlauf eine Stelle weiter (heißt es wird mit 1, 2, 4, 8 usw. verglichen).
         * Mit diesem Wert werden m_writePins `&` Verknüpft. Ist das Ergebnis nicht 0, dann heißt es, dass in m_writePins an Stelle `i` ein Bit
         * auf 1 gesetzt ist.
         *
         * Weil allerdings die Pin Nummer der konfigurierten Pins wichtig ist und nicht die Position des Bits im Byte, wird ein Counter hochgezählt.
         * Er sagt aus der wievielte Pin jetzt angesprochen wurde. Im folgenden wird er mit dem Paramter `pinNum` verglichen: `if (++bitCount == pinNum)`
         *
         * Um einen Wert auf 1 zu setzen wird logisch ODER-Verknüpft. So bleibt der Wert 1, falls er 1 war und wird 1, falls er 0 war: `m_input[1] = (byte)(m_input[1] | (byte)(1 << i))`
         * Um einen Wert auf 0 zu setzen wird logisch XOR-Verknüpft. Dann wird mit einem logischen UND geprüft ob der Wert 1 oder 0 ist (Prinzip wie oben). ist der Wert 1, sorgt das XOR,
         * dass eine 0 als Ergebnis herauskommt. Ist er 0 passiert nichts weiter.
         *
         * \param pinNum Die Nummer des Schreib-Pins, der geschrieben werden soll.
         * \param state Der Status, welcher auf den Pin geschrieben werden soll.
         * \return Liefert "true", falls die Operation erfolgreich war. "false" bei Misserfolg.
         */
        private bool setPin(int pinNum, bool state)
        {
            /* pinNum must be between 0 and 7 */
            if (pinNum >= 8 || pinNum < 0)
                return false;
            int bitCount = 0;
            for (int i = 0; i < 8; ++i)
            {
                /* the "pinNum'th" occurence is the pin which has to be set to "state" */
                if ((m_writePins & (1 << i)) != 0)
                {
                    /* checks if we iterated often enough */
                    if (++bitCount == pinNum)
                    {
                        if (state)
                        {
                            m_input[1] = (byte)(m_input[1] | (byte)(1 << i));
                        }
                        else
                        {
                            if ((m_input[1] & (1 << i)) != 0)
                                m_input[1] = (byte)(m_input[1] ^ (byte)(1 << i));
                        }
                        return true;
                    }
                }
            }
            /* no pin is set as writePin. Return false */
            return false;
        }
    }
}
