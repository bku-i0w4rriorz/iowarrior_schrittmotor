# IOWarrior Schrittmotor

Das Projekt mit dem IOWarrior Schrittmotor.  
Es geht darum einen Schrittmotor zu progammieren um mit der Position des Motors eine Uhr in WPF darzustellen

## Anforderungen an das Projekt

* Use-Case Diagramm
* Klassendiagramm
* Sequenzdiagramm
* Animierte GUI in WPF (mit C#)
* OOP (Mehrere Klassen, Vererbung etc.)
* (Threads)
* Datenbank zum Speichern von Zuständen
* Einsatz vom IOWarrior

## Aufgabenverteilung

### Simon

* [X] Klassendiagramm
* [ ] GUI (WPF)
* [ ] Programmieren

### Georg

* [X] Klassendiagramm
* [ ] Sequenzdiagramm
* [ ] Programmieren
* [ ] IOWarrior

### Robin

* [X] Klassendiagramm
* [X] Use-Case Diagramm
* [ ] Programmieren
* [ ] Datenbank

## Meilensteine

* [ ] Schrittmotor steuern
* [ ] Animierte GUI
* [ ] GUI mit Schrittmotor verbinden
* [ ] Optimierung